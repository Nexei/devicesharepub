from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

import os
import pickle

import netifaces as ni

from deviceshare.SyncServer.server import chooseInterface

SHARED_FOLDER=None
AUTHORIZER=None

def addUser(username, passwd, path_to_shared):
    AUTHORIZER.add_user(username, passwd, SHARED_FOLDER, perm='elradfmwMT') #temp perms

def runServer():
    global AUTHORIZER
    AUTHORIZER = DummyAuthorizer()
    handler = FTPHandler
    handler.authorizer = AUTHORIZER

    address = (ni.ifaddresses(chooseInterface())[ni.AF_INET][0]['addr'], 2121)
    server = FTPServer(address, handler)

    server.max_cons = 256
    server.max_cons_per_ip = 1

    server.serve_forever()

def addUsers():
    users_file_path = os.path.dirname(os.path.abspath(__file__))+"/users.pickle"
    users = None
    
    if os.path.isfile(users_file_path):
        with open(users_file_path, 'rb') as f:
            users=pickle.load(f)

    for user_cred in users:
        username, passwd = user_cred
        addUser(username, passwd)
