from socket import *
from collections import defaultdict

import os
import json
import uuid
import pickle
import threading
import platform

import netifaces as ni

MAIN_PORT=9028
BROADCAST_PORT=9027
MAIN_SOCKET=None
BROAD_SOCKET=None
GROUP_ID_PATH=None
GROUP_ID=None
GROUPS=defaultdict(set)

GROUPS_LOCK=threading.Lock()

def updateID(gid):
    global GROUP_ID
    GROUP_ID=gid
    with open(GROUP_ID_PATH, 'wb') as f:
            pickle.dump(GROUP_ID, f)

def initID():
    global GROUP_ID_PATH, GROUP_ID
    GROUP_ID_PATH=os.path.dirname(os.path.abspath(__file__))+"/gid.pickle"
    if os.path.isfile(GROUP_ID_PATH):
        print("ID FOUND")
        with open(GROUP_ID_PATH, 'rb') as f:
            GROUP_ID=pickle.load(f)
    else:
        print("ID NOT FOUND")
        updateID(str(uuid.uuid4()))

def broadcast(msg):
    global BROAD_SOCKET
    BROAD_SOCKET.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
    BROAD_SOCKET.sendto(json.dumps(msg, sort_keys=True).encode(), ('255.255.255.255', BROADCAST_PORT))

def chooseInterface():
    interface=ni.interfaces()[0]
    for t_interface in ['enp3s0', 'wlp2s0']:
        if t_interface in ni.interfaces():
            interface = t_interface
            break
    return interface

def findGroups():
    ip=ni.ifaddresses(chooseInterface())[ni.AF_INET][0]['addr']
    broadcast({"TYPE": "DISCOVER", "ADDR": ip, "GROUP_ID": GROUP_ID, "DEVICE_NAME": platform.node()})

def updateGroups():
    global GROUPS
    GROUPS = defaultdict(set)
    findGroups()

def runBroadcastServer():
    global BROAD_SOCKET, GROUPS
    BROAD_SOCKET=socket(AF_INET, SOCK_DGRAM)
    BROAD_SOCKET.bind(('', BROADCAST_PORT))

    ip=ni.ifaddresses(chooseInterface())[ni.AF_INET][0]['addr']
    while(1):
        m=BROAD_SOCKET.recvfrom(4096)  
        msg=json.loads(m[0].decode())
        if msg['TYPE'] == "DISCOVER":
            if msg["ADDR"] != ip:
                with GROUPS_LOCK:
                    GROUPS[msg['GROUP_ID']].add((msg["DEVICE_NAME"], msg["ADDR"]))
                sendMsg({"TYPE": "DISCOVER_RESP", "ADDR": ip, "GROUP_ID": GROUP_ID, "DEVICE_NAME": platform.node()}, msg["ADDR"])
        print(msg)
        print(GROUPS)

def sendMsg(msg, addr):
    global MAIN_SOCKET
    BROAD_SOCKET.sendto(json.dumps(msg, sort_keys=True).encode(), (addr, MAIN_PORT))

def runServer():
    global MAIN_SOCKET
    MAIN_SOCKET=socket(AF_INET, SOCK_DGRAM)
    MAIN_SOCKET.bind((ni.ifaddresses(chooseInterface())[ni.AF_INET][0]['addr'], MAIN_PORT))

    while(1):
        m=MAIN_SOCKET.recvfrom(4096)  
        msg=json.loads(m[0].decode())
        if msg['TYPE'] == "DISCOVER_RESP":
            with GROUPS_LOCK:
                GROUPS[msg['GROUP_ID']].add((msg["DEVICE_NAME"], msg["ADDR"]))
        print(msg)
        print(GROUPS)

