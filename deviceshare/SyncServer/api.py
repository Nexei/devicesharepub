from deviceshare.SyncServer.server import broadcast, sendMsg, updateGroups, runBroadcastServer, initID, findGroups, GROUPS, runServer

import threading

def broadcastApi(msg):
    broadcast(msg)

def sendMsgApi(msg, addr):
    sendMsg(msg, addr)

def updateGroupsApi():
    updateGroups()

def getGroupsApi():
    return GROUPS.copy()

def runServerApi():
    print("runSERVERAPI")
    initID()
    threading.Thread(target=runBroadcastServer).start()
    threading.Thread(target=runServer).start()
    findGroups()
